package com.example.terp.watchtest;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.URL;

/**
 * Created by terp on 29-6-16.
 */
class BackgroundConnection extends AsyncTask<Void,Void,String> {

    protected String doInBackground(Void... Params) {
        String ServerMessage="Nil";
        try{
            // Creating new socket connection to the IP (first parameter) and its opened port (second parameter)
            Socket s = new Socket("10.0.2.2", 9090);
            // Initialize output stream to write message to the socket stream
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
            String outMsg = "";
            outMsg = "Hello from mobile";
            // Write message to stream
            out.write(outMsg);
            // Flush the data from the stream to indicate end of message
            out.flush();
            // Close the output stream
            out.close();
            // Close the socket connection
            s.close();
            ServerMessage="Succes!";
        }

        catch(Exception ex){
            //Handle exceptions
            ex.printStackTrace();
        }
        return ServerMessage;
    }

    protected void onPostExecute(String result){
        String TAG = "ConnectionLog";
        /* output result */
        Log.i(TAG,result);
        //todo fancy output
    }
}
