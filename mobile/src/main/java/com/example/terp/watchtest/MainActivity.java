package com.example.terp.watchtest;

import com.example.terp.watchtest.*;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Click0(View view){
        Toast.makeText(MainActivity.this, "Pressed (0)", Toast.LENGTH_SHORT).show();
    }

    public void Click1(View view){
        Toast.makeText(MainActivity.this, "Pressed (1)", Toast.LENGTH_SHORT).show();
    }

    public void ClickQuestion(View view){
        Toast.makeText(MainActivity.this, "Pressed (?)", Toast.LENGTH_SHORT).show();
        new BackgroundConnection().execute();
    }

}
