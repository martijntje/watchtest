package com.example.terp.watchtest;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

class BackgroundConnection extends AsyncTask<Void,Void,String> {

    protected String doInBackground(Void... Params) {
        String ServerMessage="Nil";
        try{
            //import test


            //end import test


            ServerMessage="No change";
            // Creating new socket connection to the IP (first parameter) and its opened port (second parameter)
            Socket s = new Socket("10.0.2.2", 9090);
            // Initialize output stream to write message to the socket stream
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
            BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String outMsg = "";
            outMsg = "Hello from watch";

            out.write("Hello, this is the android wear app\n");

            out.write("What is the first challenge?\n");
            out.flush();
            int challenge1 = Integer.parseInt( in.readLine() );

            out.write("What is the second challenge?\n");
            out.flush();
            int challenge2 = Integer.parseInt( in.readLine() );

            int response = challenge1 * challenge2;

            out.write("The response is: " + response +"\n");
            out.flush();

            in.close();
            out.close();
            s.close();

        }

        catch(Exception ex){
            //Handle exceptions
            ex.printStackTrace();
        }
        return ServerMessage;
    }

    protected void onPostExecute(String result){
        String TAG = "ConnectionLog";
        /* output result */
        Log.i(TAG,result);
        //todo fancy output
    }
}
